interface Account {
  attributes: {
    "id": number,
    "user_id": number,
    "name": string,
    "balance": number
  }
}

export { Account }
