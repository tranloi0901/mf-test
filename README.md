# mf-test

## Build Setup

```bash
# install dependencies
$ npm install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm run start

# generate static project
$ npm run generate

# run test case
$ npm run test
```

## Env
* duplicate .env.sample to .env file and change .env file content
