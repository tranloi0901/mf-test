import { GetterTree, ActionTree, MutationTree } from 'vuex'
import { apiInstance, apiUrls } from '@/config/api';
import { setUrlValueParams} from '@/utils';
import { Account } from '@/models/account';
import axios from 'axios';

const getApi = async (request: any) => {
  const params = request.params;
  const query = params.query ? params.query : {};
  const response = await apiInstance.get(setUrlValueParams(request), { params: query });
  return response.data;
}

export const state = () => ({
  user: [] as Account[],
})

export type RootState = ReturnType<typeof state>

export const getters: GetterTree<RootState, RootState> = {}

export const mutations: MutationTree<RootState> = {}

export const actions: ActionTree<RootState, RootState> = {
  async getUserAccounts({ commit }, params : any): Promise<any> {
    try {
      return getApi({
        url: apiUrls.userAccounts,
        params: params
      });
    } catch (error) {
      return error;
    }
  },
  async getAccount({ commit }, params : any): Promise<any> {
    try {
      return getApi({
        url: apiUrls.account,
        params: params
      });
    } catch (error) {
      return error;
    }
  },
  async getUser({ commit }, params : any): Promise<any> {
    try {
      return getApi({
        url: apiUrls.user,
        params: params
      });
    } catch (error) {
      return error;
    }
  }
}
