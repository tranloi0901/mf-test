import { actions } from './users';
import axios from 'axios';
jest.mock('axios');

test('return list accounts by specific user id', async () => {
  const mockData = [
    {
      "attributes": {
        "id": 1,
        "user_id": 1,
        "name": "A銀行",
        "balance": 20000
      }
    },
    {
      "attributes": {
        "id": 3,
        "user_id": 1,
        "name": "C信用金庫",
        "balance": 120000
      }
    },
    {
      "attributes": {
        "id": 5,
        "user_id": 1,
        "name": "E銀行",
        "balance": 5000
      }
    }
  ];
  axios.get.mockResolvedValue({
    data: mockData
  });

  const response = await actions.getUserAccounts({}, {
    id: 1
  });
  expect(response.length).toEqual(mockData.length);
  expect(response[0]['attributes'].title).toEqual(mockData[0]['attributes'].title);
});

test('return detail user info', async () => {
  const mockData = {
    "attributes":
    {
        "id": 1,
        "name": "Alice",
        "account_ids": [1,3,5]
    }
  };

  axios.get.mockResolvedValue({
    data: mockData
  });

  const response = await actions.getUser({}, {
    id: 1
  });
  expect(response['attributes'].title).toEqual(mockData['attributes'].title);
});

test('return detail account info', async () => {
  const mockData = {
    "attributes": {
      "id": 2,
      "user_id": 2,
      "name": "Bカード",
      "balance": 200
    }
  };

  axios.get.mockResolvedValue({
    data: mockData
  });

  const response = await actions.getAccount({}, {
    id: 2
  });
  expect(response['attributes'].title).toEqual(mockData['attributes'].title);
  expect(response['attributes'].user_id).toEqual(mockData['attributes'].user_id);
  expect(response['attributes'].balance).toEqual(mockData['attributes'].balance);
});
