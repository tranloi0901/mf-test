export default {
  // Disable server-side rendering: https://go.nuxtjs.dev/ssr-mode
  ssr: false,

  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: 'mf-test',
    htmlAttrs: {
      lang: 'en'
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' },
      { name: 'format-detection', content: 'telephone=no' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
    'element-ui/lib/theme-chalk/index.css',
    '~/css/main.scss'
  ],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    '@/plugins/element-ui',
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/typescript
    '@nuxt/typescript-build',
    '@nuxtjs/dotenv'
  ],

  axios: {
    // baseURL: process.env.NODE_ENV === 'production' ? process.env.PRODUCT : process.env.STAGING
    baseURL: process.env.BASE_API,
    proxyHeaders: false,
    credentials: false
  },

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    '@nuxtjs/axios',
    [
      'nuxt-i18n',
      {
        detectBrowserLanguage: false
      }
    ],
  ],
  i18n: {
    locales: [
      {
        code: 'ja',
        file: 'ja-JP.js',
        name: '日本語'
      },
      {
        code: 'en',
        file: 'en-US.js',
        name: 'English'
      }
    ],
    lazy: true,
    langDir: 'lang/',
    defaultLocale: 'en',
    fallbackLocale: 'en'
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
    transpile: [/^element-ui/],
  },

  router: {
    extendRoutes(routes, resolve) {
      routes.push({
        name: '/',
        path: '/',
        component: resolve(__dirname, 'pages/users/index.vue')
      });
    },
  }
}
