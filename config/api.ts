import axios from 'axios';
axios.defaults.baseURL = process.env.BASE_API;
axios.defaults.withCredentials = false;

export const apiInstance = axios;

export const apiUrls = {
  user: 'users/:id',
  userAccounts: 'users/:id/accounts',
  account: 'accounts/:id',
};
