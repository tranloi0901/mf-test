export default {
  user: 'users/getUser',
  userAccounts: 'users/getUserAccounts',
  account: 'users/getAccount',
};
