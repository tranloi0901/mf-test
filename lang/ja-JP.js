export default {
  id: 'Id',
  name: 'Name',
  balance: 'Balance',
  search: 'Search',
  accounts: 'Accounts',
  user_id: 'user_id',
};
