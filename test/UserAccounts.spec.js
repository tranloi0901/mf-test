import Vuex from "vuex";
import { mount, createLocalVue } from "@vue/test-utils";
import axios from 'axios';
import Accounts from '@/pages/users/_slug/accounts';
import Vue from 'vue';
import Element from 'element-ui';
Vue.use(Element, {});
const localVue = createLocalVue();
localVue.use(Vuex);

const mockData = [
  {
    "attributes": {
      "id": 1,
      "user_id": 1,
      "name": "A銀行",
      "balance": 20000
    }
  },
  {
    "attributes": {
      "id": 3,
      "user_id": 1,
      "name": "C信用金庫",
      "balance": 120000
    }
  },
  {
    "attributes": {
      "id": 5,
      "user_id": 1,
      "name": "E銀行",
      "balance": 5000
    }
  }
];

describe("Get user accounts when load page", () => {
  let store;
  let actions;
  beforeEach(async () => {
    actions = {
      getUserAccounts: jest.fn(() => Promise.resolve(mockData)),
    }
    store = new Vuex.Store({
      actions
    })
  });
  const wrapper = mount(Accounts, {
    localVue,
    store,
    error: '',
    accounts: [],
    mocks: {
      $t: (msg) => msg,
      $store: store
    },
  })
  test('page contains label id, name, balance', async () => {
    const wrapperText = wrapper.text().toLowerCase();
    expect(wrapperText).toContain('id');
    expect(wrapperText).toContain('name');
    expect(wrapperText).toContain('balance');
  });

  test("list valid user accounts", async () => {
    const response = await store.dispatch("getUserAccounts", {
      id: 1,
      query: {}
    });
    expect(response.length).toEqual(3);
  });
});
