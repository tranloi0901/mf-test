import { mount, createLocalVue } from '@vue/test-utils';
import VueRouter from 'vue-router';
import UserSearch from '@/pages/users';
import Vue from 'vue';
import Element from 'element-ui';
Vue.use(Element, {});
const localVue = createLocalVue()
localVue.use(VueRouter)
const router = new VueRouter()

const factory = (values = {}) => {
  return mount(UserSearch, {
    data () {
      return {
        ...values
      }
    },
    localVue,
    router,
    mocks: {
      $t: (msg) => msg
    }
  })
}
describe('Search Form', () => {
  let wrapper
  beforeEach(() => {
    wrapper = factory()
  })
  test('has the expected input element', () => {
    expect(wrapper.find('input[name="search"]').exists()).toBeTruthy();
  })
  test('has the expected button search', () => {
    expect(wrapper.find('button').exists()).toBeTruthy();
  })
  test('disable search button when load page', () => {
    expect(wrapper.find('button[disabled="true"]')).toBeTruthy();
  })
  test('handle click search button', async () => {
    await wrapper.find('input[name="search"]').setValue('1')
    await wrapper.get('button').trigger('click');
    global.window = Object.create(window);
    const url = "/users/1/accounts";
    Object.defineProperty(window, 'location', {
      value: {
        href: url
      }
    });
    expect(window.location.href).toEqual(url);
  })
})
